//
//  ContentView.swift
//  StrokeWinterSchool
//
//  Created by Nicolas Degen on 16.03.20.
//  Copyright © 2020 Echo Labs AG. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State var selection = 0
  let a = false
  var body: some View {
    Group {
      if a {
        
        TabView(selection: $selection) {
          NavigationView {
            Form {
              Section {
                NavigationLink(destination: PDFView(name: "D")) {
                  Text("D")
                }
              }
              Section {
                NavigationLink(destination: PDFView(name: "B")) {
                  Text("B")
                }
              }
            }
            .listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, .regular)
            .navigationBarTitle("Brochures")
          }
          .tag(0)
          .tabItem {
            VStack {
              Image(systemName: "doc")
              Text("D")
            }
          }
          NavigationView {
            Image("Map")
              .navigationBarTitle("Map")
          }
          .tag(1)
          .tabItem {
            VStack {
              Image(systemName: "map")
              Text("Map")
            }
          }
        }.edgesIgnoringSafeArea(.top)
      } else {
        TabView(selection: $selection) {
          PDFView(name: "D")
            //            Color.red
            .tag(0)
            .tabItem {
              VStack {
                Image(systemName: "doc")
                Text("D")
              }
          }
          PDFView(name: "B").tag(1).tabItem {
            VStack {
              Image(systemName: "doc.text")
              Text("B")
            }
          }
        }.edgesIgnoringSafeArea(.top)
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

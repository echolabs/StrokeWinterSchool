//
//  PDFView.swift
//  StrokeWinterSchool
//
//  Created by Nicolas Degen on 16.03.20.
//  Copyright © 2020 Echo Labs AG. All rights reserved.
//

import SwiftUI
import PDFKit

struct PDFView: UIViewRepresentable {
  var name: String
  func makeUIView(context: Context) -> UIView {
    let view = PDFKit.PDFView()
    
    
    if let path = Bundle.main.url(forResource: name, withExtension: "pdf"),
      let document = PDFDocument(url: path) {
      view.document = document
    }
    return view
  }
  
  func updateUIView(_ uiView: UIView, context: Context) {
  }
}

